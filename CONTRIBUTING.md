# Contributing to HealthBerry

Thank you for considering contributing to HealthBerry! We appreciate your interest in making this open source project even better. Whether you're a developer, designer, tester, or just passionate about health tech, there are several ways you can contribute.

## Ways to Contribute

- **Reporting Issues:** If you come across a bug, have a suggestion, or want to request a new feature, please visit our [issue tracker](https://gitlab.com/betty.dev/healthberry/-/issues). Before creating a new issue, please check if a similar one already exists.

- **Submitting Pull Requests:** Developers can contribute by forking our repository, creating a new branch for their changes, and then submitting a pull request for review. Please ensure that your code follows our coding standards and includes appropriate test coverage.

- **Documentation:** Clear and comprehensive documentation is important. If you're not a developer, you can still contribute by improving our documentation to help users understand and use HealthBerry effectively.

- **User Experience (UX) and Design:** Designers can help enhance the user interface and overall user experience of HealthBerry. Your insights and improvements are highly valuable.

- **Testing:** Testing is crucial to ensure the stability of HealthBerry. You can contribute by testing new features, bug fixes, and reporting any issues you encounter.

## Getting Started

1. Fork the [HealthBerry repository](https://gitlab.com/betty.dev/healthberry) to your own GitLab account.
2. Clone the forked repository to your local machine.
3. Create a new branch for your changes: `git checkout -b feature/your-feature-name`.
4. Make your changes and commit them: `git commit -am 'Add some feature'`.
5. Push the changes to your fork: `git push origin feature/your-feature-name`.
6. Open a pull request on the original HealthBerry repository, targeting the appropriate branch.

## Code Style and Guidelines

- Follow the existing code style and naming conventions used in the project.
- Write clear, concise, and meaningful commit messages.
- Include tests for new features or bug fixes. Ensure that existing tests pass.

## Issue and Pull Request Etiquette

- Always be respectful and considerate when discussing issues or reviewing pull requests.
- Provide detailed information when reporting issues or suggesting changes. The more context you provide, the easier it is for others to understand and help.

## Licensing

By contributing to HealthBerry, you agree that your contributions will be licensed under the project's [GNU License](https://gitlab.com/betty.dev/healthberry/-/blob/main/GNU%20LICENSE).

We appreciate your contributions and look forward to working together to improve HealthBerry for everyone!

If you have any questions, feel free to contact us or create an issue for discussion.
