//
//      2024  healthberry
//

import SwiftUI

@main
struct healthberryApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
