//
//  Home.swift
//  healthberry
//
//  Created by Priyal PORWAL on 16/02/24.
//

import SwiftUI

struct Home: View {
    /// View Properties
    @State private var activeTab: Tab = .dashboard

    /// All Tabs
    @State private var allTabs: [AnimatedTab] = Tab.allCases.compactMap { tab -> AnimatedTab? in
            .init(tab: tab)
    }

    @State private var tabShapePosition: CGPoint = .zero

    @Namespace private var animation

    var items: [MenuItem] {
        return bodyCategories
    }

    @State private var option: MenuItem? = nil

    let bodyCategories: [MenuItem] = [
        .init(title: "Head"),
        .init(title: "Shoulders"),
        .init(title: "Torso"),
        .init(title: "Hips"),
        .init(title: "Arms"),
        .init(title: "Legs"),
        .init(title: "Knees"),
        .init(title: "Feet")
    ]

    init() {
        UITabBar.appearance().isHidden = true
    }
    var body: some View {
        VStack(spacing: 0, content: {
            TabView(selection: $activeTab) {
                NavigationStack {
                    VStack {

                    }
                    .navigationTitle(Tab.dashboard.title)
                }
                .setUpTab(.dashboard)

                NavigationStack {
                    VStack {
                        DropdownMenu(placeholder: "Choose a body part", menuItems: items, selectedItem: $option, excludedItems: nil)
                    }
                    .frame(maxWidth: UIScreen.main.bounds.width, maxHeight: UIScreen.main.bounds.height - 300)
                    .padding()
                    .background(
                        Image("body")
                            .resizable()
                            .edgesIgnoringSafeArea(.all)
                            .aspectRatio(contentMode: .fit)
                    )
                }
                .setUpTab(.history)

                NavigationStack {
                    VStack {
                    }
                    .navigationTitle(Tab.account.title)
                }
                .setUpTab(.account)
            }
            CustomTabBar()
        })
    }

    // Custom Tab Bar
    @ViewBuilder
    func CustomTabBar(
        _ tint: Color = Color("Orange"),
        _ inactiveTint: Color = Color("Orange").opacity(0.8)) -> some View {
            HStack(alignment: .bottom, spacing: 0) {
                ForEach($allTabs) { $animatedTab in
                    TabItem(tint: tint,
                            inactiveTint: inactiveTint,
                            animatedTab: animatedTab,
                            animation: animation,
                            activeTab: $activeTab,
                            position: $tabShapePosition)
                }
            }
            .padding(.horizontal, 15)
            .padding(.vertical, 10)
            .background(content: {
                TabShape(midpoint: tabShapePosition.x)
                    .fill(.white)
                    .ignoresSafeArea()
                    .shadow(color: tint.opacity(0.2), radius: 5, x: 0, y: -5)
                    .blur(radius: 2)
                    .padding(.top, 40)
            })
            .animation(.interactiveSpring(response: 0.6, dampingFraction: 0.7, blendDuration: 0.7), value: activeTab)
        }
}

struct TabItem: View {
    var tint: Color
    var inactiveTint: Color
    @State var animatedTab: AnimatedTab
    var animation: Namespace.ID
    @Binding var activeTab: Tab
    @Binding var position: CGPoint

    // Each tab item position on screen
    @State private var tabPosition: CGPoint = .zero
    var body: some View {
        VStack(spacing: 4) {
            Image(systemName: animatedTab.tab.rawValue)
                .font(.title2)
                .frame(width: activeTab == animatedTab.tab ? 58 : 35,
                       height: activeTab == animatedTab.tab ? 58 : 35)
                .foregroundStyle(activeTab == animatedTab.tab ? .white : inactiveTint)
                .background {
                    if activeTab == animatedTab.tab {
                        Circle()
                            .fill(tint.gradient)
                            .matchedGeometryEffect(id: "ACTIVETAB", in: animation)
                    }
                }
                .symbolEffect(.bounce.down.byLayer, value: animatedTab.isAnimating)
            Text(animatedTab.tab.title)
                .font(.caption)
                .textScale(.secondary)
                .foregroundStyle(activeTab == animatedTab.tab ? tint : .gray)
        }
        .frame(maxWidth: .infinity)
        .padding(.top, 15)
        .padding(.bottom, 10)
        .contentShape(.rect)
        .viewPosition(completion: { rect in
            tabPosition.x = rect.midX
            // Updating active tab position
            if activeTab == animatedTab.tab {
                position.x = rect.midX
            }
        })
        .onTapGesture {
            withAnimation(.bouncy, completionCriteria: .logicallyComplete, {
                activeTab = animatedTab.tab
                animatedTab.isAnimating = true
            }, completion: {
                var transaction = Transaction()
                transaction.disablesAnimations = true
                withTransaction(transaction) {
                    animatedTab.isAnimating = nil
                }
            })
            withAnimation(.interactiveSpring(response: 0.6, dampingFraction: 0.7, blendDuration: 0.7)) {
                position.x = tabPosition.x
            }
            activeTab = animatedTab.tab
        }
    }
}
#Preview {
    ContentView()
}

extension View {
    @ViewBuilder
    func setUpTab(_ tab: Tab) -> some View {
        self.frame(maxWidth: .infinity, maxHeight: .infinity)
            .tag(tab)
            .toolbar(.hidden, for: .tabBar)
    }
}
