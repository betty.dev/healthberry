//
//  Tab.swift
//  healthberry
//
//  Created by Priyal PORWAL on 16/02/24.
//

import Foundation

enum Tab: String, CaseIterable {
    // `rawValue` used as SFSymbol Image
    case dashboard = "square.stack.3d.down.right.fill"
    case history = "menucard.fill"
    case account = "person.2.crop.square.stack.fill"

    // Tab title
    var title: String {
        switch self {
        case .dashboard:
            return "Dashboard"
        case .history:
            return "History"
        case .account:
            return "Account"
        }
    }

    // Returns current tab index
    var index: Int {
        return Tab.allCases.firstIndex(of: self) ?? 0
    }
}

/// Animated SF Tab Model
struct AnimatedTab: Identifiable {
    var id: UUID = UUID()
    var tab: Tab
    var isAnimating: Bool?
}
