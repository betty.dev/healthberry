// 2024  healthberry

import SwiftUI

struct IconTintKey: EnvironmentKey {
	static var defaultValue: Color = Color.orange
}

extension EnvironmentValues {
	var iconTint: Color {
		get { self[IconTintKey.self] }
		set { self[IconTintKey.self] = newValue }
	}
}

extension View  {
	func iconTint(_ tint: Color) -> some View {
		environment(\.iconTint, tint)
	}
}
