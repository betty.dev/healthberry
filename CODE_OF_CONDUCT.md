# HealthBerry Open Source Project - Code of Conduct

We, the HealthBerry community, are committed to creating a welcoming and inclusive environment for all contributors and participants. We value diversity and respect and aim to provide a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, personal appearance, race, religion, or sexual identity and orientation.

## Expected Behavior

- Be respectful and considerate towards others' ideas and contributions.
- Use welcoming and inclusive language.
- Be open to constructive feedback and different points of view.
- Focus on the content and ideas, not personal attacks.
- Exercise empathy and kindness in your interactions.

## Unacceptable Behavior

The following behaviors are considered unacceptable within the HealthBerry community:

- Harassment, trolling, insulting, or derogatory comments in any form.
- Discrimination, exclusion, or harmful language related to personal characteristics.
- Posting or sharing inappropriate content.
- Any form of offensive, rude, or aggressive behavior.
- Intimidation or unwelcome attention.

## Reporting and Enforcement

If you witness or experience any behavior that violates this Code of Conduct, please report it by contacting the project maintainers at [maintainers@email.com](mailto:maintainers@email.com). All reports will be treated confidentially.

Project maintainers are responsible for enforcing the Code of Conduct. They have the right and responsibility to address and remove comments, commits, code, or other contributions that violate this Code of Conduct. Instances of abusive, harassing, or otherwise unacceptable behavior may result in temporary or permanent bans from the project's community spaces.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant](https://www.contributor-covenant.org/), version 2.0, available at [https://www.contributor-covenant.org/version/2/0/code_of_conduct.html](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).
