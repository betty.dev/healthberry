//
//  ContentView.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 01.11.2023..
//

import SwiftUI

struct ContentView: View {
    @State private var selectedTab = 0
    @State private var isOnboardingShowed = true

    var body: some View {
        NavigationView {
            TabView(selection: $selectedTab) {
                    // Tab 1
                BodyScreen()
                    .tabItem {
                        Label("Me", systemImage: "figure.mind.and.body")
                    }
                    .tag(0)

                    // Tab 2
                Spacer()
                Text("Pain list")
                    .tabItem {
                        Label("Pains", systemImage: "folder")
                    }
                    .tag(1)
                Spacer()
                SettingsScreen()
                    .tabItem {
                        Label("Settings", systemImage: "gear")
                    }
                    .tag(2)
            }
            .accentColor(.black)
            .accessibilityElement(children: .ignore)
        }
        .fullScreenCover(isPresented: $isOnboardingShowed) {
            HelloScreen(isOnboardingShowed: $isOnboardingShowed)
        }
    }
}

#Preview {
    ContentView()
}
