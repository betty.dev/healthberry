//
//  CustomViews.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 03.11.2023..
//

import SwiftUI


/// Change navigation bar color and font settings with UIKit
struct NavigationBarColor: ViewModifier {

    init(backgroundColor: UIColor, tintColor: UIColor) {
        let coloredAppearance = UINavigationBarAppearance()
        coloredAppearance.configureWithTransparentBackground()
        coloredAppearance.backgroundColor = backgroundColor
        coloredAppearance.titleTextAttributes = [.foregroundColor: tintColor, .font: UIFont.systemFont(ofSize: 16, weight: .semibold) as Any]
        coloredAppearance.largeTitleTextAttributes = [.foregroundColor: tintColor, .font: UIFont.systemFont(ofSize: 30, weight: .bold) as Any]
        coloredAppearance.backButtonAppearance.normal.titleTextAttributes = [.foregroundColor: tintColor, .font: UIFont.systemFont(ofSize: 16, weight: .regular)]

        UINavigationBar.appearance().standardAppearance = coloredAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = coloredAppearance
        UINavigationBar.appearance().compactAppearance = coloredAppearance
        UINavigationBar.appearance().tintColor = tintColor
    }

    func body(content: Content) -> some View {
        content
    }
}

extension View {
    /// Extension for NavigationViw to change navigationBarColor: tint and background via modifier .navigationBarColor
    func navigationBarColor(backgroundColor: UIColor, tintColor: UIColor) -> some View {
        self.modifier(NavigationBarColor(backgroundColor: backgroundColor, tintColor: tintColor))
    }
}


