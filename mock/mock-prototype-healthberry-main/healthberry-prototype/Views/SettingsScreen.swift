//
//  SettingsScreen.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 03.11.2023..
//

import SwiftUI

struct SettingsScreen: View {
    var body: some View {
        NavigationView {
            List{
                Section(header: Text("Your access")
                    .foregroundColor(Color(.systemGray2))
                ){ //: SECTION HEADER

                        //: Notifications settings
                    NavigationLink(
                        destination: CreatePainScreen(),
                        label: {
                            Text("Subscription")
                        })//: NAVLINK

                        //: Color Theme
                    NavigationLink(
                        destination: CreatePainScreen(),
                        label: {
                            Text("Terms and conditions")
                        })
                } //: SECTION
                
                    // time
                Section(header: Text("Customisation")
                    .foregroundColor(Color(.systemGray2))
                    ){ //: SECTION HEADER

                            //: Notifications settings
                        NavigationLink(
                            destination: CreatePainScreen(),
                            label: {
                                Text("Privacy")
                            })//: NAVLINK

                            //: Color Theme
                        NavigationLink(
                            destination: CreatePainScreen(),
                            label: {
                                Text("Preferences")
                            })

                }
            }
            .navigationBarTitleDisplayMode(.large)
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle(Text("Settings"))
        }
    }
}

#Preview {
    SettingsScreen()
}
