//
//  HelloScreen.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 01.11.2023..
//

import SwiftUI

struct HelloScreen: View {
    @Binding var isOnboardingShowed: Bool

    var body: some View {
        ZStack(alignment: .leading) {
            VStack(alignment: .leading, spacing: 10){
                Text("Hello 👋")
                    .font(.title)
                    .fontWeight(.bold)
                Text("I am your new health tracker.")
                    .font(.headline)
                    .fontWeight(.medium)

                VStack(alignment: .leading, spacing: 20){
                    Group{
                        Text("My goal is to help you track your most important pains metrics and focus on building healthy routines.")

                        Text("Also, I can remind you to when to visit a doctor")
                    }

                    Text("To use all my potential and have a great experience, please, allow me to show you your pain history to help you stay on track.")


                    Button(action: {

                    }, label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 10)
                                .frame(height: 50, alignment: .center)
                                .foregroundColor(Color.black)
                            Text("allow reminders  \(Image(systemName: "bell.fill"))")
                                .foregroundColor(Color.white)
                        }
                    }) .padding(.top)

                    Button(action: {
                        isOnboardingShowed = false
                    }, label: {
                        ZStack{
                            RoundedRectangle(cornerRadius: 10)
                                .frame(height: 50, alignment: .center)
                                .foregroundColor(Color.black)
                            Text("let's start  \(Image(systemName: "arrow.right"))")
                                .foregroundColor(Color.white)
                        }
                    })

                }
                .padding(.top)
            } //: VSTACK
            .padding()
            .lineSpacing(10.0)

        } //: ZSTACK
        .transition(.opacity)
    }
}

#Preview {
   HelloScreen(isOnboardingShowed: .constant(true))
}
