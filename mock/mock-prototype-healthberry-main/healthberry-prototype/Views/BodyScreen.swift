//
//  BodyView.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 01.11.2023..
//

import SwiftUI

struct BodyScreen: View {
    @State private var isSheetPresented = false

    var body: some View {
        NavigationView {
            ZStack(alignment: .center) {
                Image("human") // Replace "backgroundImage" with your image name
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .edgesIgnoringSafeArea([.trailing, .leading])
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .clipped()
                ForEach(0..<4, id: \.self) { _ in
                    PainAnimationView()
                        .position(
                            x: CGFloat.random(in: 10...UIScreen.main.bounds.size.width - 150),
                            y: CGFloat.random(in: 10...UIScreen.main.bounds.size.height - 150)
                        )
                }
            }
            .navigationBarColor(backgroundColor: UIColor(Color.white), tintColor: UIColor(Color.black))
            .navigationBarItems(trailing: Image(systemName: "plus")
                .foregroundColor(.black)
                .font(.system(size: 24, weight: .semibold))
                .onTapGesture {
                    isSheetPresented.toggle()
                })
                //: NAVITEMS
            .navigationBarTitle(Text("Pains".uppercased()))
            .navigationBarTitleDisplayMode(.inline)
        }
        .sheet(isPresented: $isSheetPresented) {
            CreatePainScreen()
        }
    }
}

#Preview {
    BodyScreen()
}
