//
//  PainAnimationView.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 01.11.2023..
//

import SwiftUI

struct PainAnimationView: View {
    @State private var animation: Double = 0.0

    var body: some View {
        ZStack {
            Circle()
                .fill(Color.cyan)
                .frame(width: 34, height: 34, alignment: .center)
            Circle()
                .stroke(Color.cyan, lineWidth: 2)
                .frame(width: 30, height: 30, alignment: .center)
                .scaleEffect(1 + CGFloat(animation))
                .opacity(1 - animation)
//            Image(systemName: "cross.circle.fill")
//                .resizable()
//                .scaledToFit()
//                .frame(width: 28, height: 28, alignment: .center)
//                .clipShape(Circle())
//                .foregroundColor(.white)
        } //: ZSTACK
        .onAppear(perform: {
            withAnimation(.easeInOut(duration: 2).repeatForever(autoreverses: false)) {
                animation = 1
            }
        })
    }
}

#Preview {
    PainAnimationView()
}
