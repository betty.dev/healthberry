//
//  CreatePainScreen.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 03.11.2023..
//

import SwiftUI

struct CreatePainScreen: View {
    @State var openColorPanel = false
    @State var openBadgesPanel = false
    @State var isWarningClose = false
    @Environment(\.presentationMode) var presentation
    @State var name = ""

    var body: some View {
        NavigationView {
            ZStack {

                List{
                    Section(header: Text("Name your pain")) {
                        TextField("Leg, headeche, belly pain etc.", text: $name)
                    } //: SECTION

                        // time
                    Section(header: Text("When did this start")) {
                        TextField("Today", text: $name)
                            .keyboardType(.default)
                    }

                        // Pain badge state
                    Section(header: Text("Choose color")){
                        HStack{
                            Text("Badge color")
                            Spacer()
                            Button(action: {
                                withAnimation(.linear(duration: 0.2)){
                                    openColorPanel.toggle()
                                }
                            }, label: {
                                Image(systemName: "largecircle.fill.circle")
                                    .resizable()
                                    .frame(width: 25, height: 25)
                                    .foregroundColor(Color.pink)
                            })
                        }
                    } //: SECTION
                     // Badge style
                    Section(header: Text("Pick badge style")){
                        HStack{
                            Text("Badge style")
                            Spacer()
                            Button(action: {
                                withAnimation(.linear(duration: 0.2)){
                                    openBadgesPanel.toggle()
                                }
                            }, label: {
                                Image(systemName: "smallcircle.fill.circle.fill")
                                    .resizable()
                                    .frame(width: 25, height: 25)
                                    .foregroundColor(Color.cyan)
                            })
                        } //: HSTACK
                    }

                } //: LIST
                .listStyle(InsetGroupedListStyle())

            } //: ZSTACK
            .onTapGesture {
                //  hideKeyboard()
            }
            .navigationBarItems(
                leading: Button(action: {
                }, label: {
                    Text("Save")
                        .foregroundColor(.black)
                        .opacity(name.count == 0 ? 0.5 : 1)
                        .transition(.opacity)
                })
                .disabled(name.count == 0)
                ,trailing: Button(action: {
                    name.count == 0 ? presentation.wrappedValue.dismiss() : isWarningClose.toggle()
                }, label: {
                    Image(systemName: "xmark")
                        .foregroundColor(.black)
                })
            ) //: NAVITEMS
            .navigationBarTitle(Text("Note your pain"))
            .navigationBarTitleDisplayMode(.inline)



        } //: NAV
        .actionSheet(isPresented: $isWarningClose, content: {
            ActionSheet(title: Text("Are you sure you want to cancel the pain note?"), buttons: [
                .destructive(Text("Yes")){presentation.wrappedValue.dismiss()},
                .default(Text("No"))])
        })
    }
}

#Preview {
    CreatePainScreen()
}
