//
//  healthberry_prototypeApp.swift
//  healthberry-prototype
//
//  Created by Marina Huber on 01.11.2023..
//

import SwiftUI

@main
struct healthberry_prototypeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
