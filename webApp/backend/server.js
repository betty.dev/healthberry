import express from 'express'
const app = express()
const PORT = process.env.PORT || 8080
import dotenv from 'dotenv'
import { createClient } from '@supabase/supabase-js'


app.use(express.json())


dotenv.config({ path:'/.env' })

// const supabase = createClient(process.env.supabaseUrl, process.env.supabaseAnonKey);

app.use("/", (req, res) => {
    res.send("Hi from Maye!")
})

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`)
})

