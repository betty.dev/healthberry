
import { LoginForm } from "../components/authForms/LoginForm";
import SignUp from "../components/authForms/SignUp";

const HomePage = () => {
  return (
    <div>
      <LoginForm />
      <SignUp />
    </div>
  );
};
export default HomePage;