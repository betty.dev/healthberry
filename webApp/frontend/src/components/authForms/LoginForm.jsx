export const LoginForm = () => {
  return (
    <div className="wrapper">
      <form action="">
        <h1>Login</h1>
        <div className="input-box">
          <input type="text" placeholder="Username" required />
        </div>
        <div className="input-box">
          <input type="password" placeholder="Password" required />
        </div>

        <div className="remember-forgot">
          <label><input type="checkbox" />Remember me</label>
          <a href="#">Forgot password?</a>
        </div>

        <button type="submit">Login</button>
        
        <div className="register-link">
          <p>Dont have an account? <a href="#">Sign Up</a></p>
        </div>

        <div id="skip-register" className="">
          <a href="#">Want to test-drive?</a>
        </div>
      </form>
    </div>
  )
}
