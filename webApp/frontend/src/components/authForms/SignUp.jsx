const SignUp = () => {
  return (
    <div>
      <h2>SignUp</h2>

      <form action="signUp" method="post">
        <div>
          <label htmlFor="fName">First Name</label>
          <input type="text" name="fName" />
        </div>
        <div>
          <label htmlFor="lName">First Name</label>
          <input type="text" name="lName" />
        </div>
        <div>
          <label htmlFor="email">Email Address</label>
          <input type="email" name="email" />
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input type="password" name="password" />
        </div>

        <button type="submit" className="login-btn">
          Sign Up
        </button>
        <p className="mt-3 text-center">
          Already a member?
          {/* <Link to="/login">Login</Link> */}
        </p>
      </form>
    </div>
  );
};
export default SignUp;
