<h1>Security Policy</h1>

<h2>Supported Versions</h2>
<p>Health Berry is currently hosted as a centrally managed service. We ensure the security of the latest code in our main branch. Users do not need to track versions as we continuously update and maintain the service.</p>

<h3>Reporting a Vulnerability</h3>
<p>If you discover a security vulnerability in Health Berry, we appreciate your responsible disclosure.
Please create an issue <a href="https://gitlab.com/betty.dev/healthberry/-/issues/new">here</a> or contact our security team via email at [coming soon](mailto:coming soon)</p>

To help us understand and address the issue promptly, please include the following details in your report:

<ul>
<li>**Platform**: Specify the operating system and version you were using. For example: Windows 10 (64-bit)</li>
<li>**Exact Version**: Provide the exact commit or version of Health Berry that you tested. For example: commit e7d6f2c4b88a9c2e5c1d1f7f0b9a7a56c899a3f8.
</li>
<li>**Bug Details**: Describe the source location of the bug and any additional information about its cause.</li>
<li>**Browser (if relevant)**: If the vulnerability is related to a web browser, specify the browser and its version. For example: Mozilla Firefox 110.0.1 (64-bit).</li>
<li>**Site/API URL (if relevant)**: Share the URL if the issue pertains to a specific endpoint or API.
</li>
<li>**Exploitation Explanation**: Provide a clear explanation of how the vulnerability can be exploited.
</li>
</ul>

To be considered a security issue, the bug must be reproducible on the latest release of Health Berry using a realistic attack vector.

## Response

<p>Our team is committed to addressing security vulnerabilities promptly. We will acknowledge your report and work on a resolution. If necessary, we will provide updates on the status of the issue and collaborate with you on any additional information required.</p>

#### We appreciate your responsible disclosure and your assistance in keeping Health Berry secure for all users.
