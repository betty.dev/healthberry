FROM node:18-alpine
WORKDIR /webApp
COPY webApp/frontend/package.json .
RUN npm install
RUN npm i -g serve
COPY webApp/frontend ./
RUN npm run build
EXPOSE 3000
CMD ["serve", "-s", "dist"]