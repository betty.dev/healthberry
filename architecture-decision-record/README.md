# architecture-decision-record

## What is an ADR ?

Architecture decisions are records are documents written by the team regarding some architecture changes that occured in the platform.

Each document describes a context:
- Context: The list of forces (technical, environmental) that occured at some point in time.
- Decision: A response regarding those forces.
- Status: Whether the response has been accepted by the team.
- Consequences: some of the notable consequences regarding the introducing of this change.

You can find a more detailed description of an ADR in this blog post by Michael T. Nygaard [Documenting Architecture Decisions](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).


## How is this repository working ?

Each ADR is numbered incrementally based on it's announcement date.

Whenever a feature developed introduced a notable architecture change an ADR can be written and merged in this repository.
